//Task 1
function getCookingTime(eggsAmount) {
    let result;
    if (typeof eggsAmount === "number" && eggsAmount >= 0) {
        result = Math.ceil(eggsAmount / 5) * 5;
    }
    return result;
}

console.log(getCookingTime(23));

//Task 2
function getNumber(array) {
    const oddResult = array.filter(item => item % 2 !== 0);
    let evenResult = array.filter(item => item % 2 === 0);

    if (evenResult.length < oddResult.length) {
        return evenResult;
    }

    return oddResult;
}

console.log(getNumber([1, 5, 7, 9, 15, 19, 777, -15, -11, 4, 9, 23, -17]));
console.log(getNumber([0, 2, 8, -4, 0, -122, 13, -4, 28, 12]));

//Task 3
function findTitle(array, string) {
    let result = [];
    let temp;

    for (let i = 0; i < array.length; i++) {
        temp = array[i];

        if (temp.title) {
            if (temp.title.toLowerCase().includes(string)) {
                result.push(array[i]);
            }
        }
    }
    return result;
}

let arrayStrings = [{
    title: "Some title"
}, {
    title: "I like JS"
}, {
    user: "This obj doesn\`t have key title js"
}, {
    title: "Js - is the best!"
}
];

console.log(findTitle(arrayStrings, "js"));

//Task 4

function countCharacters(string) {
    const result = {};
    const temp = string.replace(/[\s.,%!’]/g, '').split('');
    // console.log(temp);
    for (let i = 0; i < temp.length; i++) {
        const item = temp[i];
        if (result[item] >=1 ) {
            result[item]++;
        } else {
            result[item] = 1;
        }
    }

    return result;
}

// let test = "sparrow";

console.log(countCharacters("sparrow"));
console.log(countCharacters("aabcdddefffge"));
console.log(countCharacters("a 2ab !d’"));

//Task 4*
function getNextPalindrome(number) {
    let result = number;
    let temp = number;
    while (true) {
        temp++;
        result = temp.toString().split('').reverse().join('');
        while (temp.toString() === result && result.length > 1) {
            return temp;
        }
    }
}

console.log(getNextPalindrome(7));
console.log(getNextPalindrome(99));
console.log(getNextPalindrome(132));
console.log(getNextPalindrome(888));
console.log(getNextPalindrome(999));

